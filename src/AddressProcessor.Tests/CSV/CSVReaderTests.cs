﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using AddressProcessing.CSV;

namespace Csv.Tests
{
    [TestFixture]
    public class CSVReaderTests
    {
        private const string TestInputFile = @"test_data\contacts.csv";
        private const string TestOutputFile = @"test_data\contacts_output.csv";
        
        [Test]
        public void CSVReader_ReadCSV_Test1()
        {
            int counter = 0;
            using (CSVReader csvR = new CSVReader())
            { 
                csvR.Open(TestInputFile);
                string[] cols = csvR.ReadColumns();               
                while (cols != null)
                {
                    Assert.IsTrue(cols.Length == 4, "number of columns should be 4");
                    counter++;
                    cols = csvR.ReadColumns();
                }
                csvR.Close();
            }
            Assert.IsTrue(counter == 229, "number of rows should be 229");
        }
    }
}
