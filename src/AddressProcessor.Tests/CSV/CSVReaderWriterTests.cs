﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using AddressProcessing.CSV;

namespace Csv.Tests
{
    [TestFixture]
    public class CSVWriterTests
    {

        private const string TestInputFile = @"test_data\contacts.csv";
        private const string TestOutputFile = @"test_data\contacts_output.csv";

        [Test]
        public void CSVReaderWriter_ReadCSV_Test1()
        {
            CSVReaderWriter csvR1 = new CSVReaderWriter();
            csvR1.Open(TestInputFile, CSVReaderWriter.Mode.Read);

            int counter = 0;
            string[] cols1 = csvR1.Read();
            while (cols1 != null)
            {
                cols1 = csvR1.Read();
                counter++;
            }
            csvR1.Close();
            
            Assert.IsTrue(counter == 229, "number of rows shoould be 229");
        }

        [Test]
        public void CSVReaderWriter_WriteCSV_Test1()
        {
            CSVReaderWriter csvR1 = new CSVReaderWriter();
            csvR1.Open(TestInputFile, CSVReaderWriter.Mode.Read);

            CSVReaderWriter csvW = new CSVReaderWriter();

            csvW.Open(TestOutputFile, CSVReaderWriter.Mode.Write);
             
            string[] cols1 =csvR1.Read();
            while (cols1 != null)
            {
                csvW.Write(cols1[0], cols1[1]);
                cols1 = csvR1.Read();
            }
            csvR1.Close();
            csvW.Close();

            CSVReaderWriter csvR2 = new CSVReaderWriter();
            csvR2.Open(TestOutputFile, CSVReaderWriter.Mode.Read);
            string[] cols2 = csvR2.Read();
            int counter = 0;
            while (cols2 != null)
            {
                counter++;
                cols2 = csvR2.Read();
            }
            
           Assert.IsTrue(counter == 229, "number of rows shoould be 229");
        }


        [Test]
        public void CSVReaderWriter_ReadCSV_Compatibility_Test1()
        {
            CSVReaderWriter csvR1 = new CSVReaderWriter();
            csvR1.Open(TestInputFile, CSVReaderWriter.Mode.Read);

            int counter = 0;
            string col1;
            string col2;
            bool result = csvR1.Read(out col1, out col2);
            while (result)
            {
                Assert.IsTrue(col1 != null && col2 != null, "column1 and column2 should have values");
                result = csvR1.Read(out col1, out col2);
                counter++;
            }
            csvR1.Close();

            Assert.IsTrue(counter == 229, "number of rows shoould be 229");
        }

    }
}
