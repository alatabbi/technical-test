﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using AddressProcessing.CSV;

namespace Csv.Tests
{
    [TestFixture]
    public class CSVReaderWriterTests
    {

        private const string TestInputFile = @"test_data\contacts.csv";
        private const string TestOutputFile = @"test_data\contacts_output.csv";


        [Test]
        public void CSVWriter_WriteCSV_Test1()
        {
            int counter = 0;
            using (CSVReader csvR1 = new CSVReader())
            {
                csvR1.Open(TestInputFile);
                using (CSVWriter csvW = new CSVWriter("\t"))
                {
                    csvW.Open(TestOutputFile);
                    string[] cols1 = csvR1.ReadColumns();
                    while (cols1 != null)
                    {
                        csvW.Write(cols1[0], cols1[1]);
                        cols1 = csvR1.ReadColumns();
                    }
                    csvW.Close();
                }
                csvR1.Close();
            }

            using (CSVReader csvR2 = new CSVReader())
            {
                csvR2.Open(TestOutputFile);
                string[] cols2 = csvR2.ReadColumns();
                while (cols2 != null)
                {
                    counter++;
                    cols2 = csvR2.ReadColumns();
                }
                csvR2.Close();
            }
            
           Assert.IsTrue(counter == 229, "number of rows shoould be 229");
        }
    }
    }
