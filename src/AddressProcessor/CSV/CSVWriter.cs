﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace AddressProcessing.CSV
{

    public class CSVWriter : IDisposable
    {
        private StreamWriter _writerStream = null;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        private string _delimiter;
        public CSVWriter(string delimiter = "\t")
        {
            _delimiter = delimiter;
        }
        public void Open(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            _writerStream = fileInfo.CreateText();
        }
        
        public void Write(params string[] columns)
        {
            WriteLine(String.Join(_delimiter.ToString(), columns));
        }

        private void WriteLine(string line)
        {
            _writerStream.WriteLine(line);
            _writerStream.Flush();
        }
        
        public void Close()
        {
            if (_writerStream != null)
            {
                _writerStream.Close();
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ((IDisposable)_writerStream).Dispose();
                }
                disposedValue = true;
            }
        }

        ~CSVWriter()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
