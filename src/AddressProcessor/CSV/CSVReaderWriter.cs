﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AddressProcessing.CSV
{
    /*
        2) Refactor this class into clean, elegant, rock-solid & well performing code, without over-engineering.
           Assume this code is in production and backwards compatibility must be maintained.
    */


    [Obsolete("Class is deprecated, for backward compatibility only, use CSVReader and CSVWriter instead.")]
    public class CSVReaderWriter: IDisposable
    {
        private StreamReader _readerStream = null;
        private StreamWriter _writerStream = null;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        private string _delimiter;
        public CSVReaderWriter(string delimiter = "\t")
        {
            _delimiter = delimiter;
        }
        public void Open(string fileName, Mode mode)
        {            
            if (mode == Mode.Read)
            {
                _readerStream = File.OpenText(fileName);
            }
            else if (mode == Mode.Write)
            {
                FileInfo fileInfo = new FileInfo(fileName);
                _writerStream = fileInfo.CreateText();
            }
            else
            {
                throw new Exception("Unknown file mode for " + fileName);
            }
        }

        #region Reader
        
        [Obsolete("Method is deprecated, for backward compatibility only")]
        public bool Read(string column1, string column2)
        {
            // I don't understand the purpose of this method

            column1 = null;
            column2 = null;

            char[] separator = _delimiter.ToCharArray();
            string[] columns  = this.Read();

            if (columns == null)
                return false;
            
            if (columns.Length < 2)
                return false;

            column1 = columns[0];
            column2 = columns[1];
            return true;

        }

        [Obsolete("Method is deprecated, for backward compatibility only")]
        public bool Read(out string column1, out string column2)
        {
            column1 = null;
            column2 = null;
            string[] columns =Read();

            if (columns == null)               
                return false;
              
            if (columns.Length < 2)
                return false;

            column1 = columns[0];
            column2 = columns[1];
            return true;

        }

        public string[] Read()
        {
            IEnumerator<string> enumr = GetEnumerator();
            if (enumr.MoveNext())
            {
                if (enumr.Current != null)
                {
                    string line = enumr.Current;
                    return line.Split(_delimiter.ToCharArray());
                } 
            }
            return null;
        }

        public IEnumerable<string> ReadLines()
        {
            do
            {
                string line = _readerStream.ReadLine();
                yield return line;
            } while (!_readerStream.EndOfStream);
        }

       
        public IEnumerator<string> GetEnumerator()
        {
            return this.ReadLines().GetEnumerator();
        }

        private string ReadLine()
        {
            IEnumerator<string> enumr = GetEnumerator();
            if (!enumr.MoveNext())
                return null;
            string line = enumr.Current;
            return line;
            
        }
        
        IEnumerable<string> GetEnumerable()
        {
            using (TextReader reader = _readerStream)
            {
                string line = _readerStream.ReadLine();
                if (line != null)
                {
                    yield return line;
                }
            }
        }

        public void Reset()
        {
            this.GetEnumerator().Reset();
        }
        #endregion

        #region Writer
        public void Write(params string[] columns)
        {            
            WriteLine(String.Join(_delimiter.ToString(), columns));
        }

        private void WriteLine(string line)
        {
            _writerStream.WriteLine(line);
            _writerStream.Flush();
        }
        #endregion

        public void Close()
        {
            if (_writerStream != null)
            {
                _writerStream.Close();
            }

            if (_readerStream != null)
            {
                _readerStream.Close();
            }
        }
        
        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ((IDisposable)_writerStream).Dispose();
                    ((IDisposable)_readerStream).Dispose();
                }
                disposedValue = true;
            }
        }

        ~CSVReaderWriter()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
