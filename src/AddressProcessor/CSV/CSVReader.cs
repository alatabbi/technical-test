﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AddressProcessing.CSV
{
    /*
        2) Refactor this class into clean, elegant, rock-solid & well performing code, without over-engineering.
           Assume this code is in production and backwards compatibility must be maintained.
    */

    public class CSVReader : IDisposable
    {
        private StreamReader _readerStream = null;

        private string _delimiter;

        public CSVReader(string delimiter = "\t")
        {
            _delimiter = delimiter;
        }

        public void Open(string fileName)
        {
            _readerStream = File.OpenText(fileName);
        }


        public string[] ReadColumns()
        {
            IEnumerator<string> enumr = GetEnumerator();
            if (enumr.MoveNext())
            {
                if (enumr.Current != null)
                {
                    string line = enumr.Current;
                    return line.Split(_delimiter.ToCharArray());
                }
            }
            return null;
        }

        public IEnumerable<string> ReadLines()
        {
            do
            {
                string line = _readerStream.ReadLine();
                yield return line;
            } while (!_readerStream.EndOfStream);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return this.ReadLines().GetEnumerator();
        }

        private string ReadLine()
        {
            IEnumerator<string> enumr = GetEnumerator();
            if (!enumr.MoveNext())
                return null;
            string line = enumr.Current;
            return line;

        }

        IEnumerable<string> GetEnumerable()
        {
            using (TextReader reader = _readerStream)
            {
                string line = _readerStream.ReadLine();
                if (line != null)
                {
                    yield return line;
                }
            }
        }

        public void Reset()
        {
            this.GetEnumerator().Reset();
        }

        public void Close()
        {
            if (_readerStream != null)
            {
                _readerStream.Close();
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                    ((IDisposable)_readerStream).Dispose();
                }
                disposedValue = true;
            }
        }

        ~CSVReader()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

